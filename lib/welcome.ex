defmodule Welcome do
  @moduledoc """
  This module can easily greets the user, but is not programmed to say goodbye.
  """

  @doc """
  This function greets the user and asks who the user is.

  ## Examples

      iex> Welcome.hello
      "Hey, hello! Who are you?"
  """
  def hello do
    "Hey, hello! Who are you?"
  end

  @doc """
  This function tells who it is.

  ## Examples

      iex> Welcome.i_am("Human")
      "Welcome, Human. I am an Elixir Module!"
  """
  def i_am(name) do
    case name do
      Null -> "Do not think you are Null, pal!"
      false -> "I feel empty sometimes too..."
      name -> "Welcome, #{name}. I am an Elixir Module!"
    end
  end

  @doc """
  This function tells who it is to a two or more persons.

  ## Examples

      iex> Welcome.we_are(["Human", "Dog"])
      "Welcome, Human and Dog! Nice to meet you!"
  """
  def we_are(names) do
    cond do
      names == Null -> "Null way!"
      names == false -> "You must tell me your names!"
      length(names) == 0 -> "Why do you not tell me your names?"
      length(names) == 1 -> "To be WE you need to give to me more than one name, #{names.first}."
      length(names) > 1 -> "Welcome, #{Enum.join(names, " and ")}! Nice to meet you!"
    end
  end

  @doc """
  This function is not yet implemented. :/

  ## Examples

      iex> Welcome.goodbye
      "I can not say goodbye, sorry."
  """
  def goodbye do
    "I can not say goodbye, sorry."
  end

  @doc """
  The module do not care, sorry.

  ## Examples

      iex> Welcome.but_i_really_need_to_go
      "I can not let you go."
  """
  def but_i_really_need_to_go do
    "I can not let you go."
  end

  @doc """
  :)

  ## Examples

      iex> Welcome.oh_crap
      "Welcome!"
  """
  def oh_crap do
    "Welcome!"
  end
end
