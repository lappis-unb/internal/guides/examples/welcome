FROM elixir:1.5

ADD . /code

WORKDIR /code

RUN mix local.hex --force && \
    mix deps.get

CMD ["iex", "-S", "mix"]
