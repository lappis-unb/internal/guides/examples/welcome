defmodule WelcomeTest do
  use ExUnit.Case
  doctest Welcome

  test "Greets the user" do
    assert Welcome.hello == "Hey, hello! Who are you?"
  end

  test "I am Null" do
    assert Welcome.i_am Null == "Do not think you are Null, pal!"
  end

  test "I am nobody" do
    assert Welcome.i_am "" == "I feel empty sometimes too..."
  end

  test "We are Null" do
    assert Welcome.we_are Null == "Null way!"
  end

  test "We are nobody" do
    assert Welcome.we_are [] == "Why do you not tell me your names?"
  end

  test "We are I" do
    assert Welcome.we_are ["Human"] == "To be WE you need to give to me more than one name, Human."
  end
end
