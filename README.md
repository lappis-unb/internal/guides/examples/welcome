# Welcome

This is an example of [Elixir](https://elixir-lang.org/) project using `mix`. The main purpose of this project is to serve as an example of project with `GitLab CI/CD` by the context of [LAPPIS](https://fga.unb.br/lappis).

There is a single lib called `welcome` with functions that greets the user, but is not programmed to say goodbye. :/

* **Usage**:

```bash
$ iex -S mix
iex> Welcome.hello()
"Hey, hello! Who are you?"
iex> Welcome.i_am("Human")
"Welcome, Human. I am an Elixir Module!"
iex> Welcome.we_are(["Human", "Dog"])
"Welcome, Human and Dog! Nice to meet you!"
iex> Welcome.goodbye()
"I can not say goodbye, sorry."
iex> Welcome.but_i_really_need_to_go()
"I can not let you go."
iex> Welcome.oh_crap()
"Welcome!"
```
